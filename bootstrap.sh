#!/bin/sh
# a small script for bootstrapping local environment.

# some sanity checks below, make sure that we have git.
if [ ! type git >/dev/null ]; then
    >&2 echo "git binary isn't available. bailing out."
    exit 1
fi

HOMESHICK_REPO="$HOME/.homesick/repos/homeshick/"
git clone git://github.com/andsens/homeshick.git $HOMESHICK_REPO
$HOMESHICK_REPO/bin/homeshick clone -fb git@bitbucket.org:kklimonda/dotfiles.git
$HOMESHICK_REPO/bin/homeshick link -f dotfiles
