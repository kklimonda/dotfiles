# homeshick initialization {{{
# we have to initialize homeshick before the rest of zsh for the
# custom completions to kick in.
HOMESHICK_SCRIPT="$HOME/.homesick/repos/homeshick/homeshick.sh"
if [ -f $HOMESHICK_SCRIPT ]; then
	source $HOMESHICK_SCRIPT
	fpath=($HOME/.homesick/repos/homeshick/completions $fpath)
fi
# }}}
# initial zsh configuration {{{
# enable colors (used for the prompt), and use the default redhat prompt
fpath=(~/.zsh/completion $fpath)
autoload -U colors compinit promptinit edit-command-line
zle -N edit-command-line
colors
compinit
promptinit

prompt redhat

# }}}

[ -d $HOME/.local/bin/ ] && export PATH=$HOME/.local/bin:$PATH
[ -d $HOME/code/git-annex.linux/ ] && export PATH=$HOME/code/git-annex.linux:$PATH

# Use vim cli mode
bindkey -v
bindkey -M vicmd "v" edit-command-line

# history settings {{{
export HISTSIZE=500000
export SAVEHIST=500000
export HISTFILE=~/.history
setopt hist_ignore_dups
# }}}
# aliases {{{
function command_exists() {
	if which $1 > /dev/null 2>&1; then
		return 0
	else
		return 1
	fi
}

function alias_if_exists() {
	local alias_=$1
	local target=$2
	command_exists $target && alias $alias_=$target || return 1
}

#alias ls='ls --color=auto'
alias_if_exists "ack" "ack-grep"

alias vi="vim"
# }}}

# set base16 colors in the expanded colorspace
#source ~/.zsh/base16-default.sh

# gnome-terminal sets TERM to xterm, which is messing up with 256 color mode
# in tmux, change it to xterm-256color.
# UPDATE: since April 2014 gnome-terminal doesn't set COLORTERM anymore, so now
# we have to set TERM unconditionally.
if [ x"$TERM" = x"xterm" ]; then
    export TERM=xterm-256color
fi

# point te default pass store to personal.
export PASSWORD_STORE_DIR=$HOME/.password-store/personal/
# copy passwords to X clipboard, so we can paste them shift+insert later.
export PASSWORD_STORE_X_SELECTION=primary
# store all vagrant machines in ~/.vagrant.d/machines/
export VAGRANT_VMWARE_CLONE_DIRECTORY="$HOME/.vagrant.d/machines"

export EDITOR="vim"
export VISUAL="vim"

# various helpers {{{
# add given IP to Java whitelist, so we can use various Java-based KVMs
function jcontrol_allow() {
	local ip=$1
	echo "https://${ip}" >> $HOME/.java/deployment/security/exception.sites
}
# }}}

# refresh DISPLAY when using X11 forwarding {{{
function update_x11_forwarding() {
    if [ -f $HOME/.cache/display.txt ]; then
        export DISPLAY=$(cat $HOME/.cache/display.txt)
    fi
}

if [ -f $HOME/.cache/display.txt ]; then
	function preexec() {
	    update_x11_forwarding
	}
fi
# }}}

[ `uname -s ` = "Darwin" ] && source $HOME/.zsh/zshrc.osx
source $HOME/.zsh/zshrc.go

export PATH="$PATH:$HOME/.rvm/bin" # Add RVM to PATH for scripting

if type -f keychain > /dev/null; then 
    eval `keychain --agents gpg,ssh --eval --quiet`
fi
