#!/bin/sh
set -e

enc_key=72F90640
sign_key=D5747FCF
# make sure that only one instance of this script is running
lock="/run/user/$(id -u)/backups.lock"
# the last successful duplicity run.
backup_timestamp="$HOME/.cache/backup.timestamp"
# how often to do backups, twice a day by default.
backup_interval=43200
sources="$HOME/code/codilime:codilime"
metered_ssids="noise-to-signal"
tarsnap_cache="$HOME/.cache/tarsnap"
tarsnap_key="$HOME/Documents/tarsnap-noise.key"
tarsnap_binary=
for tarsnap_location in /usr/bin /usr/local/bin; do
	if [ -x $tarsnap_location/tarsnap ]; then
		tarsnap_binary=$tarsnap_location/tarsnap
		break
	fi
done
if [ -z $tarsnap_binary ]; then
	echo "no tarsnap installed; exiting"
	exit 1
fi

date=`which date`
if [ `uname -s` = "Darwin" ]; then
	date=/usr/local/bin/gdate
fi

# try locking the file, and bail out if it's already running.
#/usr/bin/lockfile -r 0 $lock

function cleanup()
{
	rm -f $lock
}
trap cleanup EXIT

# check if we have network connectivity.
function _netctl_metered_connection()
{
	echo "no"
}

function _nm_metered_connection()
{
	if nmcli -t -f TYPE,STATE dev |grep -q "^gsm:connected$"; then
		echo "yes"
		return
	fi
	conn_ssid=$(nmcli -t -f TYPE,CONNECTION dev |grep wifi |cut -d':' -f2)
	for ssid in $metered_ssids; do
		if [ x"$conn_ssid" = x"$ssid" ]; then
			echo "yes"
			return
		fi
	done
	echo "no"
}

function _tarsnap_connectivity_status()
{
	local retVal
	echo 'test' |nc -w 5 betatest-server.tarsnap.com 9279
	retVal=$?
	if [ $retVal -eq 0 ]; then
		echo "reachable"
		return
	fi
	echo "unreachable"
}

function metered_connection()
{
	if [ -x /usr/bin/nmcli ]; then
		_nm_metered_connection
	else
		_netctl_metered_connection
	fi
}

if [ x`_tarsnap_connectivity_status` != "xreachable" -o x`metered_connection` = "xyes" ]; then
	exit 0
fi

# check when was the last successful duplicity run.
if [ -f $backup_timestamp ]; then
	last_backup=$(cat $backup_timestamp)
	difference=$((`$date +%s` - $last_backup))
	if [ $difference -le $backup_interval -a x$1 != x"--force" ]; then
		exit 0
	fi
fi

export GPG_AGENT_INFO="/run/user/$(id -u)/keyring/gpg:0:1"
for source in $sources; do
	path=$(echo $source |cut -d ':' -f 1)
	archive_name=$(echo $source |cut -d ':' -f 2)
	full_name="$(hostname)-${archive_name}-$($date --iso-8601=minutes)"


	# change the cwd to parent directory of backup source, and then
	# back it up with a relative path to tarsnap.
	(
	cd $(dirname $path)
	$tarsnap_binary --print-stats --keyfile $tarsnap_key --cachedir $tarsnap_cache \
		-c -f $full_name $(basename $path)
	)
done
/bin/echo `$date +%s` > $backup_timestamp
