#!/bin/sh

inside_codilime() {
    grep -q codilime /etc/resolv.conf
}

if inside_codilime; then
    ssh localhost -W $2 < /dev/stdin > /dev/stdout
else
    ssh $1 -W $2 < /dev/stdin > /dev/stdout
fi
