# if golang is installed, add it to the path
GO_BIN="/usr/local/opt/go/libexec/bin"
if [ -d "${GO_BIN}" ]; then
  export PATH=$PATH:/usr/local/opt/go/libexec/bin
fi

export GOPATH="$HOME/code/go"
if [ ! -d $GOPATH ]; then
  unset GOPATH
fi
# vim: ft=zsh
