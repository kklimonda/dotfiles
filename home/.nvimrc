source ~/.vimrc

" remap Escape key to leaving terminal mode.
tnoremap <Esc> <C-\><C-n>
