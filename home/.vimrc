" vundle initialization code {{{
filetype off

set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

Bundle 'Keithbsmiley/tmux.vim'
Bundle 'chriskempson/base16-vim'
Bundle 'gmarik/Vundle.vim'
Bundle 'tpope/vim-fugitive'
Bundle 'rodjek/vim-puppet'
Bundle 'hynek/vim-python-pep8-indent'
Bundle 'PProvost/vim-ps1'
Bundle 'mtth/scratch.vim'
Bundle 'chase/vim-ansible-yaml'
Bundle 'wting/rust.vim'
Bundle 'wting/cheetah.vim'
Bundle 'martinda/Jenkinsfile-vim-syntax'

call vundle#end()
" }}}
" color scheme {{{
set t_Co=256

if has("gui_running")
	set guioptions+=lrbmTLce
	set guioptions-=lrbmTLce
	set guioptions+=c
	set guifont=Monospace\ Regular\ 12
	set mouse=
	set guiheadroom=0
endif
" }}}
" keyboard mappings {{{
" disable arrow keys, to force usage of hjkl
inoremap  <Up>     <NOP>
inoremap  <Down>   <NOP>
inoremap  <Left>   <NOP>
inoremap  <Right>  <NOP>
noremap   <Up>     <NOP>
noremap   <Down>   <NOP>
noremap   <Left>   <NOP>
noremap   <Right>  <NOP>

map <leader>gn :bn<cr>
map <leader>gp :bp<cr>
" }}}
filetype plugin indent on

set foldmethod=marker
set modeline 				" enable parsing modeline in files
set modelines=1
set number				" display line numbers
set hidden				" hide the buffer when it's being abandoned
set ruler
set wildignore+=*.pyc,*/__pycache__/*
set splitright
set splitbelow
set directory=~/.vim/volatile/swap//
set backupdir=~/.vim/volatile/backup//
set backspace=indent,eol,start
syntax on

let g:notes_directories = ['~/Documents/notes']

" Disable loading LogiPat, which shadows :E command
let g:loaded_logipat = 1
autocmd FileType yaml setlocal ts=2 sts=2 sw=2 expandtab
